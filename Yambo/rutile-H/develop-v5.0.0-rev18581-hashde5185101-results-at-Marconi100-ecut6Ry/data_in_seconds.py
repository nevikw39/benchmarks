#!/usr/bin/env python3
# coding: utf-8
import sys

filename = sys.argv[1]

with open(filename, 'r') as rf:
    data = rf.readlines()
    
with open('sec_'+filename, 'w') as wf:
    wf.write(data[0])
    for line in data[1:]:
        values = line.strip().split()
        for i in range(len(values)):
            val = values[i].strip('s')
            if 'm' in val:
                val = val.split('m')
                val = 60*int(val[0]) + int(val[1])
                val = str(val)
            values[i] = val
        wf.write('   '.join(values)+'\n')
